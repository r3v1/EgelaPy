EgelaPy Script
------------
Python 2 script to download files from the University of the Basque Country Moodle (aka Egela)

## Motivación
Esto fue un proyecto que quise hacer para descargar todo el contenido de los cursos de Egela, cuando
empecé a tener contacto con la programación durante el grado de Ingeniería Informática. 

Como veréis, está implementado con el ya obsoleto Python 2.7 y mediante técnicas de scraping.

Para mí sorpresa, ¡probándolo en diciembre de 2020, sigue siendo funcional!

Obviamente, si ahora tuviera que reescribir el código, lo haría mucho más eficiente y con
otras librerías (por eso aparecen salto de línea a veces sin sentido, o errores con el UTF8), 
pero para ser uno de mis primeros proyectos personales, no está nada mal.

## Install

It is preferred to run from a virtual environment:

```
virtualenv venv -p python2.7
source venv/bin/activate
```


## Requisites
To run the script, this libraries have to be installed:

- [mechanize](https://github.com/python-mechanize/mechanize)
- [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/)

```
pip install mechanize beautifulsoup4 
```

## How to use it:

- Running in terminal ```python2 menu.py```.

## Usage
Once the script is executed, you have to log in into your Egela account. That's it.

![image](doc/EgelaPyUsage.png)
