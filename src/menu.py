# -*- coding: utf-8 -*-
# !/usr/bin/python2.7

import getpass
import os

import session

global browser


class menu:

    def banner(self):
        self.clean()

        print ('\033[36m\n\t___________              .__        __________        ')
        print ('\t\_   _____/  ____   ____ |  | _____ \______   \___.__.')
        print ('\t |    __)_  / ___\_/ __ \|  | \__  \ |     ___<   |  |')
        print ('\t |        \/ /_/  >  ___/|  |__/ __ \|    |    \___  |')
        print ('\t/_______  /\___  / \___  >____(____  /____|    / ____|')
        print ('\t        \//_____/      \/          \/          \/     \033[0m\n\n')

    def clean(self):
        if os.name == "nt":
            os.system("cls")
        elif os.name == "posix":
            os.system("clear")
        else:
            print('\n\033[1;31m[+]\033[0m CAN NOT CLEAN SCREEN.\n')

    def menu(self):

        salir = False
        seguir = False

        while not seguir:
            self.banner()
            print('\nIn order to use PyGela, you may consider logging in.\n')

            user = str(input('  ** \033[4;3;mUser\033[0m: '))
            passw = getpass.getpass('  ** \033[4;3;mPass\033[0m: ')  # Evita que se muestre la pass

            if user != '' or passw != '':
                seguir = True

            s = session.Session()
            self.banner()
            seguir = s.start_session(user, passw)

        while not salir:

            print('\nSelect an option:\n')
            print('[1] Print course info.')
            print('[2] Download files.')

            opt = str(input('\n>>> '))
            self.banner()

            if opt == '1':

                print('\nSelect an option:\n')
                print('[1] Print all info.')
                print('[2] Print subject info.')

                opt = str(input('\n>>> '))
                self.banner()

                if opt == '1':
                    s.printCourse()
                elif opt == '2':
                    s.printSubNames()

                    try:
                        indice = int(input('\nSelect one: ')) - 1
                        s.asig[indice].printTree()

                    except (ValueError, IndexError):
                        print('\n\033[1;31m[+]\033[0m WRONG SUBJECT SELECTED. TRY AGAIN.\n')

            elif opt == '2':

                print('\nSelect a download type:\n')
                print('[1] All files from all subjects.')
                print('[2] Entire subject ')
                print('[3] File(s) from subject.')

                opt = str(input('\n>>> '))
                self.banner()

                s.getSubjects()
                if opt == '1':
                    for a in s.asig:
                        a.getFiles()
                        print('\n\033[1;33m[*]\033[0m Wait: downloading \033[3m%s\033[0m' % a.asignatura)
                        a.downloadFiles()

                elif opt == '2':
                    s.printSubNames()
                    try:
                        indice = int(input('\nSelect one: ')) - 1
                        print('\n\033[1;33m[*]\033[0m Wait: downloading...\n')
                        s.asig[indice].downloadFiles()

                    except (ValueError):
                        print('\n\033[1;31m[!]\033[0m WRONG SUBJECT SELECTED. TRY AGAIN.\n')
                    except (IndexError):
                        print('\n\033[1;31m[!]\033[0m NO OBJECT WITH SELECTED INDEX. TRY AGAIN.\n')


                elif opt == '3':
                    s.printSubNames()
                    try:
                        indice = int(input('\nSelect one: ')) - 1
                        s.asig[indice].printFilesInfo()

                        indice2 = int(input('\nSelect one: ')) - 1
                        s.asig[indice].getSubFiles()

                        print('\033[1;33m[*]\033[0m WAIT: downloading \033[3m%s\033[0m' % s.asig[indice].subfiles[
                            indice2].nombre)

                        s.asig[indice].subfiles[indice2].downloadFile()

                    except (ValueError):
                        print('\n\033[1;31m[!]\033[0m WRONG SUBJECT SELECTED. TRY AGAIN.\n')
                    except (IndexError):
                        print('\n\033[1;31m[!]\033[0m NO OBJECT WITH SELECTED INDEX. TRY AGAIN.\n')

        salir = str(input('\n[!] Exit? [y/n]: '))
        if salir != 'n':
            salir == True


if __name__ == '__main__':
    try:
        m = menu()
        m.menu()
    except KeyboardInterrupt:
        print('\n\033[1;31m[!]\033[0m EXITING...\n')
