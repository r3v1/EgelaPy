# -*- coding: utf-8 -*-
# !/usr/bin/python2.7

import cookielib
import os
import shutil
import sys

import mechanize
from bs4 import BeautifulSoup


class Session:

    def __init__(self):
        self.PagPrincipal = ''
        self.br = mechanize.Browser()
        self.asig = []

    def start_session(self, user, passw):
        '''Inicia sesion'''

        try:
            cj = cookielib.LWPCookieJar()
            self.br.set_cookiejar(cj)
            self.br.set_handle_robots(False)
            self.br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
            self.br.addheaders = [('User-agent',
                                   'Mozilla/5.0 (X11; U; Linux i686; es-VE; rv:1.9.0.1)Gecko/2008071615 Debian/6.0 '
                                   'Firefox/9')]

            self.br.open('https://egela.ehu.eus/login/index.php')

            for link in self.br.links():
                if link.text == "sesion":
                    self.br.open("%s" % link.absolute_url)
                    break

            self.br.select_form(nr=0)

            self.br.form['username'] = user
            self.br.form['password'] = passw

            self.br.submit()
            self.PagPrincipal = self.br.response().read()

            if self.getUser() is not None:

                print('\n\033[1;32m[+]\033[0m SESSION STARTED\n')
                print('     User : \033[4;3;32m%s\033[0m\n' % self.getUser())

            else:
                raise NoUserFound('No user found. Check user or pass.')

            return True
        except NoUserFound:
            print('\n\033[1;31m[!]\033[0m CHECK USERNAME OR PASSWORD. Press enter\n')
            raw_input()
            return False

        except Exception:
            print('\n\033[1;31m[!]\033[0m SESSION DIED\n')
            sys.exit(127)

    def getSubjects(self):
        ''' Devuelve las asginaturas disponibles '''

        # Recoge el codigo
        b = BeautifulSoup(self.PagPrincipal, 'html.parser')
        asigs = b.find_all('h3')

        asignaturas = []

        for asignatura in asigs:

            try:
                # Crear objeto Subject
                s = Subject(asignatura.a.text, asignatura.a.get('href'), self.br)
                asignaturas.append(s)
            except:
                pass

        self.asig = asignaturas
        return asignaturas

    def getBr(self):
        return self.br

    def printCourse(self):

        self.getSubjects()

        for a in self.asig:
            a.printTree()

    def getUser(self):

        pag = BeautifulSoup(self.PagPrincipal, 'html.parser')

        for a in pag.find_all('a'):
            try:
                if a.get('href').find('profile.php') > 0 and a.text.find(' ') >= 0:
                    return a.text
            except:
                pass

    def printSubNames(self):

        self.getSubjects()

        i = 1
        print('\033[1;33m## %s SUBJECTS ##\033[0m' % (self.getUser().split(' ')[0] + "'s"))
        for s in self.asig:
            print('[%s] ' % i + s.asignatura)
            i = i + 1


class NoUserFound(Exception):
    def __init__(self, message):
        self.message = message


class Subject:
    ''' Clase asignatura '''

    def __init__(self, asignatura, link, browser):
        self.link = link
        self.asignatura = asignatura
        self.archivos = []
        self.browser = browser
        self.subfiles = []

    def getFiles(self):
        archivos = []
        pagina_asign = BeautifulSoup(self.browser.open(self.link), 'html.parser')  # Con una especifica por ejemplo

        for p in pagina_asign.find_all('a'):

            # Agregar al array SOLO los archivos como pdf, carpetas o asignaciones. Nada de otros enlaces tipo SALIR
            if p.get('href') != None:
                if p.get('href').find('resource') > 0:

                    f = File(p.text.replace('Archivo', ''), p.get('href'), self.browser, self.asignatura)
                    archivos.append(f)

                elif p.get('href').find('folder') > 0:
                    f = Folder(p.text.replace('Carpeta', ''), p.get('href'), self.browser, self.asignatura)
                    archivos.append(f)

                elif p.get('href').find('assign') > 0:
                    f = Assign(p.text, p.get('href'), self.browser, self.asignatura)
                    archivos.append(f)

        self.archivos = archivos  # Array de objetos
        return archivos

    def printTree(self):

        self.getFiles()

        print('\n\033[1;32m%s\033[0m' % self.asignatura)
        for f in self.archivos[:-1]:
            f.printInfo()

        print('└── '.decode('utf-8', errors='ignore') + self.archivos[-1].nombre)

    def printFilesInfo(self):

        self.getSubFiles()

        print('\n\033[1;32m%s\033[0m' % self.asignatura)
        i = 1
        for f in self.subfiles:
            print('[%s] %s' % (i, f.nombre))
            i = i + 1

    def getSubFiles(self):

        self.getFiles()
        sub = []

        for f in self.archivos:
            if isinstance(f, File):
                sub.append(f)

            elif isinstance(f, Folder):
                f.getFiles()
                for ff in f.archivos:
                    sub.append(ff)

        self.subfiles = sub
        return sub

    def downloadFiles(self):

        self.getSubFiles()

        for f in self.subfiles:
            f.downloadFile()


class Folder:

    def __init__(self, nombre, link, browser, asignatura):
        self.nombre = nombre
        self.link = link
        self.archivos = []
        self.browser = browser
        self.asignatura = asignatura

    def getFiles(self):

        archivos = []
        pagina_asign = BeautifulSoup(self.browser.open(self.link), 'html.parser')

        for p in pagina_asign.find_all('a'):
            # Agregar al array SOLO los archivos como pdf, carpetas o asignaciones. Nada de otros enlaces tipo SALIR
            if p.get('href') != None and p.get('href').find('lang=') == -1 and p.get('href').find('pluginfile') > 0:

                if p.get('href').find('assign') > 0:
                    f = Assign(p.text, p.get('href'), self.browser, self.asignatura)
                    archivos.append(f)

                else:  # p.get('href').find('resource') > 0:
                    f = File(p.text.replace('Archivo', ''), p.get('href'), self.browser, self.asignatura)
                    archivos.append(f)

        self.archivos = archivos  # Array de objetos FILE y ASSIGNMENT, no FOLDER

    def printInfo(self):
        print('├── '.decode('utf-8', errors='ignore') + self.nombre)
        self.printFilesInfo()

    def printFilesInfo(self):

        if len(self.archivos) == 0:
            self.getFiles()
        i = 1
        for f in self.archivos:

            if i == len(self.archivos):
                print('│   └── '.decode('utf-8', errors='ignore'), '\033[1;33m%s\033[0m' % f.nombre)

            else:
                print('│   ├── '.decode('utf-8', errors='ignore'), '\033[1;33m%s\033[0m' % f.nombre)

                i = i + 1

    def downloadFile(self):
        self.getFiles()
        for f in self.archivos:
            f.downloadFile()


class File:

    def __init__(self, nombre, link, browser, asignatura):
        self.nombre = nombre
        self.browser = browser
        self.link = link
        self.asignatura = asignatura

    def downloadFile(self):

        # formatos = ['doc', 'docx', 'ppt','pptx','xls','xlsx','odd','txt','rtf','pdf','png','jpg','jpeg','rar','zip','7z','csv']

        if self.link.find('forcedownload') > 0:
            self.startDownload(self.nombre, self.link)

        else:
            pag_file = BeautifulSoup(self.browser.open(self.link), 'html.parser')

            if pag_file.text.find('PDF') > 0:
                self.startDownload(self.nombre[:-1] + '.pdf', self.link)

            elif len(pag_file) > 0:
                for p in pag_file.find_all('a'):
                    if p.get('href') != None and p.get('href').find('lang=') == -1 and p.get('href').find(
                            'pluginfile') > 0:
                        self.startDownload(p.text, p.get('href'))
            else:
                self.startDownload(self.nombre, self.link)

    def startDownload(self, nameFile, linkFile):
        ''' Creo que se puede hacer mas eficiente este algoritmo, comprobando en el metodo
            llamador si existe, y no comprobarlo aqui '''

        if not os.path.exists(os.getcwd() + '/' + self.asignatura):
            os.mkdir(os.getcwd() + '/' + self.asignatura)

        new_path = os.getcwd() + '/' + self.asignatura

        if not os.path.exists(new_path + '/' + nameFile):
            try:
                # Descarga el archivo
                tmp = self.browser.retrieve(linkFile)[0]
                shutil.move(tmp, new_path)
                os.rename(new_path + '/' + tmp.split('/')[-1], new_path + '/' + nameFile)
            except:
                print(new_path + '/' + tmp.split('/')[-1], new_path + '/' + nameFile)

            print('\033[1;32m[+]\033[0m DOWNLOAD: \033[3m%s\033[0m' % nameFile)
        else:
            print('\033[1;34m[*]\033[0m ALREADY DOWNLOADED: \033[3m%s\033[0m' % nameFile)

    def printInfo(self):
        print('├── '.decode('utf-8', errors='ignore') + self.nombre)


class Assign:

    def __init__(self, nombre, link, browser, asignatura):
        self.nombre = nombre
        self.link = link
        self.browser = browser
        self.asignatura = asignatura

    def getMark(self):

        asignacion = BeautifulSoup(self.browser.open(self.link), 'html.parser')
        i = 0

        for a in asignacion.find_all('td'):
            # Algoritmo no estable
            if a.text == ('Calificación').decode('utf-8', errors='ignore'):
                nota_s = asignacion.find_all('td')[i + 1].text
                try:
                    return float(nota_s.replace(' ', '').replace(',', '.').split('/')[0])
                except (ValueError):
                    return nota_s

            i = i + 1

    def printInfo(self):
        print('├── '.decode('utf-8', errors='ignore') + self.nombre)
        self.printAssignInfo()

    def printAssignInfo(self):

        print('│   └── '.decode('utf-8',
                                errors='ignore'), '\033[1;32mCalificación:\033[0m',
              '\033[1;3;32m%s\033[0m' % self.getMark())
        print('│'.decode('utf-8', errors='ignore'))
